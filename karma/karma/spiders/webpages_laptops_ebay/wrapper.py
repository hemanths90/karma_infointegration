import BeautifulSoup
import pdb
import csv

f = open('data.csv', 'a')
try:
	writer = csv.writer(f)
	writer.writerow( ('item condition', 'Seller', 'Location', 'number of items sold') )

	for i in range(1,1000):
		Soup = BeautifulSoup.BeautifulSoup
		tempStr = "scrapy_webpages"+str(i)+".html"
		print i
		soup = Soup(open(tempStr))
		#pdb.set_trace()
	
		#price = soup.findAll('div',{'id':'mainContent'})[0].find('span',{'id':'prcIsum'}).contents

		itm_cond = soup.findAll('div',{'id':'mainContent'})[0].find('div',{'id':'vi-itm-cond'}).contents
		#pdb.set_trace()

		#shipping_cost = soup.findAll('div',{'id':'mainContent'})[0].find('div',{'id':'shippingSummary'}).find('span',{'id':'fshippingCost'}).find('span').contents

		seller = soup.findAll('div',{'id':'RightSummaryPanel'})[0].find('span',{'class':'mbg-nw'}).contents
		#feedback = soup.findAll('div',{'id':'RightSummaryPanel'})[0].find('div',{'id':'si-fb'}).contents
		nSold = soup.findAll('div',{'id':'mainContent'})[0].find('div',{'id':'why2buy'}).find('span').contents

		location = soup.findAll('div',{'id':'mainContent'})[0].find('div',{'id':'itemLocation'}).find('div',{'class':'iti-eu-bld-gry '}).contents


		writer.writerow((str(itm_cond[0]), str(seller[0]), str(location[0]), str(nSold[0])))
finally:
    f.close()

