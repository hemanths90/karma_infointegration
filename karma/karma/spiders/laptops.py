from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.spiders import CrawlSpider,Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider
import pdb
import sys

nPages=0;

class DmozSpider(CrawlSpider):
    name = "laptops"
    allowed_domains = ["ebay.com"]
    start_urls = [
        "http://www.ebay.com/sch/i.html?_from=R40&_trksid=p2051541.m570.l1313.TR10.TRC0.A0.H0.Xlaptops.TRS0&_nkw=laptops&_sacat=58058",
    ]
    rules = [Rule(LinkExtractor(allow=(['/itm/', '_pgn=']), unique=True), callback="parse_items", follow=True)]

    def parse_items(self, response):
	#pdb.set_trace()
	global nPages;

	page_title = response.xpath("//head/title/text()").extract()
	price = response.xpath("/html/body[@class=' vi-contv2  lhdr-ie- vi-hd-ops ']/div[@id='Body']/div[@id='CenterPanelDF']/div[@id='CenterPanel']/div[@id='CenterPanelInternal']/div[@id='LeftSummaryPanel']/div[@id='mainContent']/form/div[@class='c-std vi-ds3cont-box-marpad']/div[@class='actPanel  vi-noborder ']/div[@class='u-cb']/div[@id='prcIsum-lbl']/text()").extract()
	delivery = response.xpath("/html/body[@class=' vi-contv2  lhdr-ie- vi-hd-ops ']/div[@id='Body']/div[@id='CenterPanelDF']/div[@id='CenterPanel']/div[@id='CenterPanelInternal']/div[@id='LeftSummaryPanel']/div[@id='mainContent']/form/div[@id='deliverySummary']/div[@class='u-flL lable']/text()").extract()
	item_specifics = response.xpath("/html/body[@class=' vi-contv2  lhdr-ie- vi-hd-ops ']/div[@id='Body']/div[@id='BottomPanelDF']/div[@id='BottomPanel']/div[@class='tabbable']/div[@class='tab-content-m']/div[@class='tab-pane active']/div[@class='vi-VR-tabCnt']/div[@id='vi-desc-maincntr']/div[@class='itemAttr']/div[@class='section']/h2[@class='secHd']/text()").extract()

	if page_title and price and delivery and item_specifics and nPages<1000:
		nPages +=1
		print "--------------------"+str(nPages)+"----------------------"
		with open('scrapy_webpages'+str(nPages)+'.html', "w") as f:
			f.write(response.body)
	elif nPages>1000:
		raise CloseSpider('1000 pages exceeded')
